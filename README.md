# gojs-test

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

##Removing go.js watermark

1. Go to go.js and go-debug.js inside node_modules/gojs/release.

2. Search for 7eba17a4ca3b1a8346 or 78a118b7 

    The line should look similar to 
    a.cr=b.W[Za("7eba17a4ca3b1a8346")][Za("78a118b7")](b.W,Ak,4,4);
   

3. Change it to 
    a.cr=function(){return true;};

    Do this in both go.js and go-debug.js


